import React from 'react';
import ReactDOM from 'react-dom';
import Board from './Board';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import appReducers from './reducers';
import logger from 'redux-logger';

let store = createStore(appReducers,applyMiddleware(logger));


ReactDOM.render(
    <Provider store={store} >
    	<Board />
    </Provider>,
	document.getElementById('root')
)


