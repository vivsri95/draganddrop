import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ItemTypes } from './constants';
import { DragSource } from 'react-dnd';
import { connect } from 'react-redux';
import { moveKnight } from './actions';

const knightSource = {
  beginDrag(props) {
    // console.log(props)
    return {cardId:props.cardId,cardPos:props.x};
  },
  endDrag(props, monitor) {
    const item = monitor.getItem()
    const dropResult = monitor.getDropResult()

    if (dropResult) {
      // alert(`You dropped ${item.cardId} into ${dropResult.cardPos}!`)
      // console.log("==============",item,dropResult) 
      props.dropCard({dropResult:dropResult})
      return {dropResult:dropResult}
    }
  }
};



function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
    didDrop: monitor.didDrop(),
    result:monitor.getItem()
  }
}

class Knight extends Component {

  constructor(props){
    super(props);
    this.state={cardId:this.props.cardId, cardPos:this.props.cardPos}
  }
  
  render() {
    // console.log(this.props.data)
    const { x, cardId, connectDragSource, isDragging, didDrop } = this.props;
    // if(isDragging && didDrop){
    //   console.log(this.props)
    //   this.props.move({cardPos:x,cardId:cardId})
    // }
    return connectDragSource(
      <div style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 25,
        fontWeight: 'bold',
        cursor: 'move'
      }} className="Knight">
        <div style={{ width:'80%', height:'50px', border:'1px solid', margin:'10px auto', padding:'10px'}}>
            <p style={{ fontSize:'16px' }} >ID: {this.props.data.data.id}</p>
        </div>
      </div>
    );
  }
}

Knight.propTypes = {
  connectDragSource: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired
};

const mapstateToProps = (state) =>{
  return{
  }
}


const mapDispatchToProps = (dispatch) => {
  return{
    move: (data) => dispatch(moveKnight(data))
  }
}
export default DragSource(ItemTypes.KNIGHT, knightSource, collect)(connect(mapstateToProps,mapDispatchToProps)(Knight));