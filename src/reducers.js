import { combineReducers } from 'redux';
import { MOVE_KNIGHT, DROP_KNIGHT } from './actions';


function drop(state=[],action){
	switch(action.type){
		case DROP_KNIGHT:
			return[
				...state,
				{
					drop:true,
					dropPos:action.pos

				}
			]
		default:
			return state
	}
}


const appReducers = combineReducers({
	drop
})

export default appReducers;