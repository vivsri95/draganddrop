import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Square from './Square';
import { ItemTypes } from './constants';
import { DropTarget } from 'react-dnd';

const squareTarget = {
  drop(props,monitor) {
    // console.log('drop target',props)
    return {cardPos:props.x,card:monitor.getItem()}
    // moveKnight(props.x,[itemid['id']]);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    dropCardId:monitor.getItem(),
    didDrop:monitor.didDrop()
  };
}

class BoardSquare extends Component {
  constructor(props){
    super(props)
    this.state={isDropped: this.props.didDrop}
  }

  render() {
    const { x, connectDropTarget, isOver } = this.props;
    // console.log(isOver)
    return connectDropTarget(
      <div style={{
        position: 'relative',
        width: '100%',
        height: '100%'
      }} className="boardSquare">
        <Square name={this.props.name}>
          {this.props.children}
        </Square>
        {isOver &&
          <div style={{
            position: 'absolute',
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            zIndex: 1,
            opacity: 0.5,
            backgroundColor: 'yellow',
          }} />
        }
      </div>
    );
  }
}

BoardSquare.propTypes = {
  x: PropTypes.number.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  isOver: PropTypes.bool.isRequired
};

export default DropTarget(ItemTypes.KNIGHT, squareTarget, collect)(BoardSquare);
