import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Square extends Component {
  render() {
    

    return (
      <div style={{ textAlign:'center' }}>
        <h5>{this.props.name}</h5>
        <div style={{
          backgroundColor: 'white',
          width: '100%',
          margin:'10px',
          height: '100%',
          border:'1px solid'
        }} className="square">

          {this.props.children}
          <div style={{width:'100%', height:'100px'}}>
            
          </div>
        </div>
      </div>
    );
  }
}

Square.propTypes = {
  black: PropTypes.bool
};