import React, { Component } from 'react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import PropTypes from 'prop-types';
import Square from './Square';
import Knight from './Knight';
import BoardSquare from './BoardSquare';
import { connect } from 'react-redux';
import { dropKnight } from './actions'; 
import axios from 'axios';


class Board extends Component {
  constructor(props){
    super(props)
    this.state={data:[]}
    this.dropCard = this.dropCard.bind(this)
  }

  componentWillMount(){
   try{
     axios.get('http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/opportunities?access_token=dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c')
      .then((response)=> {
        // console.log(response.data.data);
        this.initialMapping(response.data.data)
      })
      .catch((error) => {
        // console.log(error);
      });
   }
   catch(error){
    return console.log(error)
   }

  }

  renderPiece(x,y,i,data) {
    // console.log(x,y,i)
    const { cards, cardsId } = this.state
    const isKnightHere = x === this.state.knightPosition
      if(x===i){
         return <Knight x={x} cardId={y} dropCard={this.dropCard} data={data}/>
      }      
  }


  patchData(data,dropResult)  {

    let prevData = this.state.data
    for(let i = 0; i<prevData.length;i++){
      if(JSON.stringify(data.id)===JSON.stringify(prevData[i].data.id)){
        prevData[i].data = data
        prevData[i].cardPos = dropResult.dropResult.cardPos
      }
    }

    this.setState({data:prevData})
  }

  

  updateState(data,dropResult){
    for(let i=0;i<data.length;i++){
      let makeObj = {cardId:data[i].cardId,cardPos:data[i].cardPos}
      if(JSON.stringify(makeObj)==JSON.stringify(dropResult.dropResult.card)){
        data[i] = {cardId:data[i].cardId,cardPos:dropResult.dropResult.cardPos,data:data[i].data}
        this.setState({data:data})
      }
    }
  }

  initialMapping(data){
    let mapData = data.map((item,index)=>({cardId:index,cardPos:0,data:item}))
    this.setState({data:mapData})
  }

  mappingData(data,dropResult){
    let mapData = data.map((item,index)=>({cardId:index,cardPos:0,data:item}))
    this.updateState(mapData,dropResult)

  }

  dropCard(dropResult){
    let updateData;
    let patchData;
    for(let i=0;i<this.state.data.length;i++){
      let makeObj = {cardId:this.state.data[i].cardId,cardPos:this.state.data[i].cardPos}
      if(JSON.stringify(makeObj)==JSON.stringify(dropResult.dropResult.card)){
        updateData = this.state.data;
        if(dropResult.dropResult.cardPos==0){
          patchData={id:updateData[i].data.id,title:"Work"}
        }
        else if(dropResult.dropResult.cardPos==1){
          patchData={id:updateData[i].data.id,title:"Work in Progress"}
        }
        else if(dropResult.dropResult.cardPos==2){
          patchData={id:updateData[i].data.id,title:"Work Done"}
        }
      }
    }

    // updateData = updateData.map(item => (item.data))
    let ACCESS_TOKEN = 'dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c'

    let makeData ={ "access_token": `${ACCESS_TOKEN}`, "appointment_id":patchData.id, "opportunity": {title: patchData.title} }
    try{
      axios.patch(`http://gisapi-web-staging-1636833739.eu-west-1.elb.amazonaws.com/v2/opportunities/${patchData.id}`,makeData)
      .then((response)=> {
        this.patchData(response.data,dropResult)
      })
      .catch((error) => {
        console.log(error);
      });
    }
    catch(error){
      console.log(error);
    }



  }

  renderSquare(i,name) {
    const x = i % 3;
    const z = 'coming from here';
    let cards=this.state.data

    return (
      <div key={i} style={{ width: '25%' }} >

      <BoardSquare x={i} name={name}>
        {cards.map(item =>(this.renderPiece(item.cardPos,item.cardId,i,item)))}
      </BoardSquare>
        

         

      </div>
    );
  }
  

  render() {
    const squares = [];
    const name = ['Work','Work in Progress','Done']
    for (let i = 0; i < 3; i++) {
      squares.push(this.renderSquare(i,name[i]));
    }
    
    return (

      <div className="boardWrapper">
        <h5 style={{ textAlign:'center' }}>title: </h5>
        <div style={{
          width: '100%',
          display: 'flex',
          flexWrap: 'wrap',
          display:'flex',
          justifyContent:'space-around',
        }} className="Board">

          {squares}

        </div>
      </div>
    );
  }
}

Board.propTypes = {
  // knightPosition: PropTypes.number.isRequired
};

const mapStateToProps = (state) => {
  return{
    drop: state.drop
  };
};
const mapDispatchToProps = (dispatch) => {
  return{
    drop: (data) => dispatch(dropKnight(data))
  }
}


export default DragDropContext(HTML5Backend)(connect(mapStateToProps,mapDispatchToProps)(Board));