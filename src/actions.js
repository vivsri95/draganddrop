export const MOVE_KNIGHT = 'MOVE_KNIGHT';
export const DROP_KNIGHT = 'DROP_KNIGHT';

export function moveKnight(move) {
	return {
		type: MOVE_KNIGHT, 
		move
	}
}

export function dropKnight(pos){
	return{
		type:DROP_KNIGHT,
		pos
	}
}